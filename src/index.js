"use strict";

require("./index.html");
require("./bulma.css");
require("./styles.css");

const words = [
  { text: "björn", video: require("../assets/words/bjorn-00010-tecken.mp4") },
  {
    text: "elefant",
    video: require("../assets/words/elefant-01374-tecken.mp4")
  },
  { text: "får", video: require("../assets/words/far-04143-tecken.mp4") },
  { text: "buffel", video: require("../assets/words/buffel-04456-tecken.mp4") },
  { text: "apa", video: require("../assets/words/apa-01539-tecken.mp4") },
  {
    text: "bläckfisk",
    video: require("../assets/words/blackfisk-10934-tecken.mp4")
  },
  { text: "bock", video: require("../assets/words/bock-03714-tecken.mp4") },
  { text: "delfin", video: require("../assets/words/delfin-00823-tecken.mp4") },
  {
    text: "flodhäst",
    video: require("../assets/words/flodhast-01624-tecken.mp4")
  }
];

const { Elm } = require("./Main.elm");

const mountNode = document.getElementById("main");
const app = Elm.Main.init({ node: mountNode, flags: { words: words } });
