module Main exposing (..)

import Browser
import Html exposing (Html, Attribute, div, h1, input, text, ul, li, video, button, p, strong)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Random
import Random.List
import Random.Extra


-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type QuestionView
    = AnswerQuestion QuestionModel
    | CheckAnswer QuestionModel


type alias Model =
    { currentQuestion : Maybe QuestionView
    , allWords : List Word
    , questions : List QuestionModel
    }


type QuestionType
    = TextQuestionWithVideoAlternatives
    | VideoQuestionWithTextAlternatives


type alias QuestionModel =
    { word : Word
    , alternatives : List Word
    , selectedAlternative : Maybe Word
    , questionType : QuestionType
    }


type alias Word =
    { text : String
    , video : String
    }


type alias Flags =
    { words : List Word
    }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { currentQuestion = Nothing
      , allWords = flags.words
      , questions = []
      }
    , Random.generate NewQuestions (createNewQuestions flags.words)
    )


createNewQuestions : List Word -> Random.Generator (List QuestionModel)
createNewQuestions allWords =
    allWords
        |> Random.List.shuffle
        |> Random.andThen
            (\shuffledWords ->
                shuffledWords
                    |> List.map (\w -> createNewQuestionFromWord w shuffledWords)
                    |> Random.Extra.combine
            )


createNewQuestionFromWord : Word -> List Word -> Random.Generator QuestionModel
createNewQuestionFromWord word allWords =
    allWords
        |> Random.List.shuffle
        |> Random.map2
            (\questionType shuffledWords ->
                let
                    alternatives =
                        shuffledWords |> List.filter (\w -> w /= word) |> List.take 3
                in
                    { word = word
                    , alternatives = word :: alternatives
                    , selectedAlternative = Nothing
                    , questionType = questionType
                    }
            )
            randomQuestionType
        |> Random.andThen
            (\question ->
                question.alternatives
                    |> Random.List.shuffle
                    |> Random.map ((\shuffledAlternatives -> { question | alternatives = shuffledAlternatives }))
            )


randomQuestionType : Random.Generator QuestionType
randomQuestionType =
    Random.uniform VideoQuestionWithTextAlternatives [ TextQuestionWithVideoAlternatives ]



-- UPDATE


type Msg
    = SelectAlternative ( QuestionModel, Word )
    | NewQuestions (List QuestionModel)
    | ShowCheckAnswer QuestionModel
    | ShowNextQuestion


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SelectAlternative ( question, selectedWord ) ->
            let
                newCurrentQuestion =
                    { question | selectedAlternative = Just selectedWord }
            in
                ( { model | currentQuestion = Just (AnswerQuestion newCurrentQuestion) }, Cmd.none )

        NewQuestions newQuestions ->
            update ShowNextQuestion { model | questions = newQuestions }

        ShowCheckAnswer questionModel ->
            ( { model | currentQuestion = Just (CheckAnswer questionModel) }, Cmd.none )

        ShowNextQuestion ->
            let
                newQuestion =
                    model.questions
                        |> List.head
                        |> Maybe.map (\q -> (AnswerQuestion q))

                newQuestions =
                    model.questions
                        |> List.drop 1
            in
                ( { model | currentQuestion = newQuestion, questions = newQuestions }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    case model.currentQuestion of
        Just question ->
            case question of
                AnswerQuestion questionModel ->
                    case questionModel.questionType of
                        TextQuestionWithVideoAlternatives ->
                            viewTextQuestionWithVideoAlternatives questionModel

                        VideoQuestionWithTextAlternatives ->
                            viewVideoQuestionWithTextAlternatives questionModel

                CheckAnswer questionModel ->
                    viewCheckQuestion questionModel

        Nothing ->
            div [ class "container" ]
                [ h1 [ class "title" ] [ text "Det var sista frågan" ]
                , p [] [ strong [] [ text "Hur kändes det? " ], text "Förhoppningsvis så lägger vi in fler tecken och fler frågetyper framöver." ]
                ]


viewCheckQuestion : QuestionModel -> Html Msg
viewCheckQuestion questionModel =
    let
        isCorrect =
            case questionModel.selectedAlternative of
                Just selectedAlternative ->
                    selectedAlternative == questionModel.word

                Nothing ->
                    False

        isCorrectTitle =
            if isCorrect then
                "Rätt! 🎉"
            else
                "Fel."

        isCorrectSubtitle =
            if isCorrect then
                "Tecknet för \"" ++ questionModel.word.text ++ "\" är:"
            else
                "Det rätta tecknet för \"" ++ questionModel.word.text ++ "\" är:"
    in
        div [ class "container Question" ]
            [ h1 [ class "Question-title title" ] [ text isCorrectTitle ]
            , h1 [ class "subtitle" ] [ text isCorrectSubtitle ]
            , div [ class "columns" ]
                [ div [ class "column is-half is-offset-one-quarter" ]
                    [ video
                        [ src questionModel.word.video
                        , autoplay True
                        , loop True
                        , Html.Attributes.attribute "muted" ""
                        , Html.Attributes.attribute "playsinline" ""
                        , Html.Attributes.attribute "oncanplay" "this.muted=true"
                        , class "Feedback-video"
                        ]
                        []
                    ]
                ]
            , div
                [ class "columns is-centered"
                ]
                [ div
                    [ class "column has-text-centered is-2"
                    ]
                    [ button
                        [ class "button is-primary"
                        , onClick ShowNextQuestion
                        ]
                        [ text "Visa nästa fråga" ]
                    ]
                ]
            ]


viewTextQuestionWithVideoAlternatives : QuestionModel -> Html Msg
viewTextQuestionWithVideoAlternatives question =
    let
        isAnswerSelected =
            case question.selectedAlternative of
                Just _ ->
                    True

                Nothing ->
                    False
    in
        div [ class "container Question" ]
            [ h1 [ class "title" ] [ text ("Vilket av dessa tecken betyder \"" ++ question.word.text ++ "\"?") ]
            , div []
                [ viewVideoAlternatives question
                ]
            , div [ class "columns is-centered" ]
                [ div [ class "column has-text-centered is-2" ]
                    [ button [ class "button is-primary", onClick (ShowCheckAnswer question), disabled (not isAnswerSelected) ] [ text "Kolla svar" ]
                    ]
                ]
            ]


viewVideoAlternatives : QuestionModel -> Html Msg
viewVideoAlternatives question =
    div [ class "Alternatives" ]
        (List.map
            (\a ->
                video
                    [ src a.video
                    , autoplay True
                    , loop True
                    , Html.Attributes.attribute "muted" ""
                    , Html.Attributes.attribute "playsinline" ""
                    , Html.Attributes.attribute "oncanplay" "this.muted=true"
                    , class
                        ("Alternatives-alternative"
                            ++ case question.selectedAlternative of
                                Just selectedAlternative ->
                                    if selectedAlternative == a then
                                        " is-selected"
                                    else
                                        ""

                                Nothing ->
                                    ""
                        )
                    , onClick (SelectAlternative ( question, a ))
                    ]
                    []
            )
            question.alternatives
        )


viewVideoQuestionWithTextAlternatives : QuestionModel -> Html Msg
viewVideoQuestionWithTextAlternatives question =
    let
        isAnswerSelected =
            case question.selectedAlternative of
                Just _ ->
                    True

                Nothing ->
                    False
    in
        div [ class "container Question" ]
            [ h1 [ class "title" ] [ text ("Vad betyder det här tecknet?") ]
            , div [ class "columns" ]
                [ div [ class "column is-half is-offset-one-quarter" ]
                    [ video
                        [ src (question.word.video)
                        , autoplay True
                        , loop True
                        , Html.Attributes.attribute "muted" ""
                        , Html.Attributes.attribute "playsinline" ""
                        , Html.Attributes.attribute "oncanplay" "this.muted=true"
                        , class "Question-video-question"
                        ]
                        []
                    ]
                ]
            , div []
                [ viewTextAlternatives question
                ]
            , div [ class "columns is-centered" ]
                [ div [ class "column has-text-centered is-2" ]
                    [ button [ class "button is-primary", onClick (ShowCheckAnswer question), disabled (not isAnswerSelected) ] [ text "Kolla svar" ]
                    ]
                ]
            ]


viewTextAlternatives : QuestionModel -> Html Msg
viewTextAlternatives question =
    div [ class "Alternatives" ]
        (List.map
            (\a ->
                div [ class "Alternatives-alternative" ]
                    [ button
                        [ onClick (SelectAlternative ( question, a ))
                        , class
                            ("button is-large is-fullwidth"
                                ++ case question.selectedAlternative of
                                    Just selectedAlternative ->
                                        if selectedAlternative == a then
                                            " is-selected is-info"
                                        else
                                            ""

                                    Nothing ->
                                        ""
                            )
                        ]
                        [ text (capitalize a.text) ]
                    ]
            )
            question.alternatives
        )


capitalize : String -> String
capitalize string =
    let
        firstCharacter =
            string |> String.left 1 |> String.toUpper

        rest =
            string |> String.dropLeft 1
    in
        firstCharacter ++ rest
